#ifndef ALERTS_HPP_
#define ALERTS_HPP_

#include <Arduino.h>

/*		Alert
 *
 *		Base Class for all Alerts
 */
class Alert {
  public:
	Alert() 	{}
	~Alert() 	{}

	virtual void signal() { Serial.print(" Alert::signal() "); }
};

//	#################################
//	#	ALERT IMPLEMENTATIONS		#
//	#################################

/*		TerminalAlert : Alert
 *			Implementation for Alert that prints to Serial. For testing Triggers.
 */
class TerminalAlert : public Alert {
  protected:
	String message;
  public:
	TerminalAlert(String m) : message(m) {}
	~TerminalAlert() {}
	
	void signal();
};

/*		Buzzer : Alert
 *			Implementation for a buzzer. 
 *			Also works as an LED blinker, set level to .
 */
class Buzzer : public Alert {
  protected:
	byte outputPin;
	int level;
  public:
	Buzzer()				: outputPin(NULL), level(0) {}
	Buzzer(byte p)			: outputPin(p), level(50) {pinMode(outputPin,OUTPUT);}
	Buzzer(byte p, int l)	: outputPin(p), level(l) {pinMode(outputPin,OUTPUT);}
	~Buzzer() {}

	void signal();

	void volume(int l)	{ level = l; }
	int	volume() 		{ return level; }
};

/*		Mosquito : Alert
 *			Implementation for MQTT client as Alert. 
 *
 */
#include <SPI.h>
#include <PubSubClient.h>
#include <Ethernet.h>
#include "mqttclient.hpp"
class Mosquito : public Alert {
  protected:
	MQTTClient mqtt;
	char*	channel;
	char*	host;
	int		port;
  public:
	Mosquito(char c[], char h[], int p);
	~Mosquito() {}
	
	void signal();
	void signal(char message[]);
};

	
#endif /*ALERTS_HPP_*/




