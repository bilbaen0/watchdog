#ifndef ALARM_HPP_
#define ALARM_HPP_

#include "alerts.hpp"
#include "triggers.hpp"

class Alarm {
  protected:
	Trigger*	trigger;
	Alert*		alert;
	String		name;
  public:
	Alarm(Trigger* t, Alert* a, String n) : trigger(t), alert(a), name(n) {}
	~Alarm() {}
	
	void update();
	void status();
	
	String title() { return name; }
	void title(String t) { name = t; }
};

void
Alarm::update() {
	if 	( trigger->read() < trigger->threshold() ) { alert->signal(); } else { return; }
}

void
Alarm::status() {
	Serial.print("	"); Serial.print(name); Serial.print("::status() -- \n");
	Serial.print(	"		Alarm::trigger::read():	");	trigger->read();
	Serial.print("\n		Alarm::alert::signal():	");	alert->signal();
	Serial.print("\n");
}

#endif /*ALARM_HPP_*/
