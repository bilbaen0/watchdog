
#include "monitor/monitor.hpp"

//	Initialize Monitor
Monitor monitor(20);

//	Initialize Hardware
LaserTripwire	laserA(A1, 200);
LaserTripwire	laserB(A2, 300);
MotionSensor	motion(A3, A4, 15);
Buzzer			buzz(A0);

//	Link hardware to Alarms

Alarm	alarmA(&laserA, &buzz, "laserA alarm");
Alarm	alarmB(&laserB, &buzz, "laserB alarm");
Alarm	alarmC(&motion, &buzz, "motion alarm");

void
setup() {
	Serial.begin(9600);
	monitor.addAlarm(&alarmA);
	monitor.addAlarm(&alarmB);
	monitor.addAlarm(&alarmC);
	monitor.list();

	Serial.print(" -- END setup() -- \n\n"); delay(5000);
}

void
loop() {
	monitor.updateAll();
	Serial.print(" -- END LOOP() -- \n\n"); delay(5000);
}
