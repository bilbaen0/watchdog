#ifndef TRIGGERS_HPP_
#define TRIGGERS_HPP_

#include <Arduino.h>

/*		Trigger
 *			Base Class for all Triggers
 */
class Trigger {
  protected:
	byte 	inputPin;
	int		level;
  public:
	Trigger() 				: inputPin(NULL), level(0) { pinMode(inputPin, INPUT); }
	Trigger(byte p)			: inputPin(p), level(0) { pinMode(inputPin, INPUT); }
	Trigger(byte p,int iT)	: inputPin(p), level(iT) { pinMode(inputPin, INPUT); }

	~Trigger() {}
	
	virtual int read()	{ return 0; }
	
	void input(byte p) 		{ inputPin = p; }
	byte input() 			{ return inputPin; }
	void threshold(int l)	{ level = l; }
	int	threshold() 		{ return level; }
};

//	#################################
//	#	TRIGGER IMPLEMENTATIONS		#
//	#################################

/* 		LaserTripwire: Trigger
 *			Implementation for a laser trpwire.
 */
class LaserTripwire : public Trigger {
  public:
	LaserTripwire(byte p) : Trigger(p, 300) {}
	~LaserTripwire() {}

	int read() { return analogRead(inputPin); }
};

/*		MotionSensor : Trigger
 *			Implementation for a motion sensor.
 */
class MotionSensor : public Trigger {
  protected:
	byte trigPin;
  public:
	MotionSensor(byte p, byte tP);
	~MotionSensor() {}
	
	int read();
};

#endif /*TRIGGERS_HPP_*/
