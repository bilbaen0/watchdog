/*
 *		triggers.cpp
 *
 *		Author: bilbaeno
 */

#include "triggers.hpp"

/*
 *		MotionSensor : Trigger
 */
MotionSensor::MotionSensor(byte p, byte t) : Trigger(p, 15), trigPin(t) {
	pinMode(trigPin, OUTPUT);
	digitalWrite(trigPin, LOW);
}

int
MotionSensor::read() {
	digitalWrite(trigPin, HIGH);
	delay(10);
	digitalWrite(trigPin, LOW);
	delay(1000);
	int duration = pulseIn(inputPin, HIGH);
	int distance = (duration/2) / 29.1;
	
	//	TEST PRINTS
	/*Serial.print(" ->MotionSensor::read -- distance: "); Serial.print( distance );
	Serial.print(" cm\n");*/
	
	return distance;
}
