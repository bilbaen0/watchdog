
#include "alerts.hpp"

/*		TerminalAlert : Alert
 *			 Member Functions
 */
void TerminalAlert::signal () {
	Serial.print("	TerminalAlert->signal: ");
	Serial.print(message);
	Serial.print("\n");
}

/*		Buzzer : Alert
 *			Member Functions
 */
void
Buzzer::signal() {
	//Serial.print(" ->Buzzer::signal()\n"); // TEST PRINT LINE
	analogWrite(outputPin, 100);
	delay(50);
	analogWrite(outputPin, 0);
}

/*		Mosquito : Alert
 *			Member Functions
 */
Mosquito::Mosquito(char c[], char h[], int p) {
	channel = c;
	host = h;
	port = p;
	mqtt.begin(host, port, OPEN, NULL, NULL, NULL);
}

void
Mosquito::signal() {
	Serial.print(" ->Mosquito::signal()\n"); // TEST PRINT LINE
	mqtt.publish(channel, "watchDog sent a signal!");
}

void
Mosquito::signal(char message[]) {
	Serial.print(" ->Mosquito::signal()\n"); // TEST PRINT LINE
	mqtt.publish(channel, message);
}
